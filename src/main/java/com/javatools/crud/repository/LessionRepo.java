package com.javatools.crud.repository;

import com.javatools.crud.model.Lession;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LessionRepo extends JpaRepository<Lession,Integer> {
}
