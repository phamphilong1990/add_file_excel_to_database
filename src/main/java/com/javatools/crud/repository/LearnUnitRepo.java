package com.javatools.crud.repository;

import com.javatools.crud.model.LearnUnit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LearnUnitRepo extends JpaRepository<LearnUnit,Integer> {
}
