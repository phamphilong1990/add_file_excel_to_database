package com.javatools.crud.repository;

import com.javatools.crud.model.LearnItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LearnItemRepo extends JpaRepository<LearnItem,Integer> {
}
