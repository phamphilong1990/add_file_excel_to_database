package com.javatools.crud.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "learn_units")
public class LearnUnit {
    @Id
    @Column(name = "id")
    private int id;
    @Column(name = "lession_id")
    private int lessionId;
    @Column(name = "type")
    private String type;
    @Column(name = "name_native_language")
    private String nameNativeLanguage;
    @Column(name = "name_forgein_language")
    private String nameForgeinLanguage;
    @Column(name = "learn_item_id")
    private String learnItemId;
    @Column(name = "created_at")
    private Date createdAt;
    @Column(name = "updated_at")
    private Date updatedAt;
}
