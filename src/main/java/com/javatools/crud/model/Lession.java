package com.javatools.crud.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "lessions")
public class Lession {

    @Id
    @Column(name = "id")
    private int id;
    @Column(name = "course_id")
    private int courseId;
    @Column(name = "name_native_language")
    private String nameNativeLanguage;
    @Column(name = "name_second_language")
    private String nameSecondLanguage;
    @Column(name = "image")
    private String image;
    @Column(name = "type")
    private String type;
    @Column(name = "created_at")
    private Date createdAt;
    @Column(name = "updated_at")
    private Date updatedAt;
}
