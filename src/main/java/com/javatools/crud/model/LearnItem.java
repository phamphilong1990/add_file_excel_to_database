package com.javatools.crud.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "learn_items")
public class LearnItem {

    @Id
    @Column(name = "id")
    private int id;
    @Column(name = "learn_unit_id")
    private int learnUnitId;
    @Column(name = "type")
    private String type;
    @Column(name = "content")
    private String content;
    @Column(name = "score")
    private int score;
    @Column(name = "created_at")
    private Date createdAt;
    @Column(name = "updated_at")
    private Date updatedAt;
}
