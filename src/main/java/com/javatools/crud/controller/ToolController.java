package com.javatools.crud.controller;


import com.javatools.crud.model.LearnItem;
import com.javatools.crud.model.LearnUnit;
import com.javatools.crud.model.Lession;
import com.javatools.crud.service.LearnItemService;
import com.javatools.crud.service.LearnUnitService;
import com.javatools.crud.service.LessionService;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.formula.eval.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class ToolController {
    @Autowired
    private LearnItemService learnItemService;
    @Autowired
    LearnUnitService learnUnitService;
    @Autowired
    private LessionService lessionService;



    @GetMapping("/index")
    public String indexDm() {

        return "index";
    }

    @RequestMapping(value = "/toolDeleteAll",method= {RequestMethod.DELETE, RequestMethod.GET})
    public String deleteAll(Model model){
        lessionService.deleteAllLession();// delete dữ liệu bảng lession
        learnUnitService.deleteAllLearnUnit();// delete dữ liệu bảng learnUnit
        learnItemService.deleteAllLearnItem();// delete dữ liệu bảng learnItem

        model.addAttribute("message", "delete success");
        return "testDemo";
    }

    @RequestMapping(value = "/toolAddFromExcel",method = {RequestMethod.POST})
    public String addAll(@RequestParam MultipartFile myFile, Model model) {
        model.addAttribute("message", "add success");
        try {
            List<Lession> listLession = lessionService.readLessionFromExcel(myFile);// đọc dữ liệu excel trả về danh sách lession
            lessionService.saveLessionList(listLession);// lưu danh sách lession vào database

            List<LearnUnit> listLearnUnit = learnUnitService.readLearnUnitFromExcel(myFile);// đọc dữ liệu excel trả về danh sách learnUnit
            learnUnitService.saveLearnUnitList(listLearnUnit);// lưu danh sách learnUnit vào database

            List<LearnItem> listLearnItem = learnItemService.readLearnItemFromExcel(myFile);// đọc dữ liệu excel trả về danh sách learnItem
            learnItemService.saveLearnItems(listLearnItem);// lưu danh sách learnItem vào database
        }catch (NotImplementedException notImplementedException){
            notImplementedException.printStackTrace();
            model.addAttribute("message", notImplementedException.getCause());
            model.addAttribute("message1", "add fail");
        }catch (DataIntegrityViolationException dataIntegrityViolationException){
            model.addAttribute("message",dataIntegrityViolationException.getCause());
            model.addAttribute("message1","add fail");
        }
        return "testDemo";

    }
}
