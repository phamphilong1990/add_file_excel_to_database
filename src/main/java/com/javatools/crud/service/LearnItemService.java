package com.javatools.crud.service;


import com.javatools.crud.model.LearnItem;
import com.javatools.crud.repository.LearnItemRepo;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.eval.NotImplementedException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Slf4j
@Service
public class LearnItemService {

    @Autowired
    private LearnItemRepo learnItemRepo;

    public List<LearnItem> saveLearnItems(List<LearnItem> learnItems) throws DataIntegrityViolationException {
        return learnItemRepo.saveAll(learnItems);
    }

    public String deleteAllLearnItem() {
        learnItemRepo.deleteAll();
        return "delete all";
    }

    public List<LearnItem> readLearnItemFromExcel(MultipartFile myFile) throws NotImplementedException {
        List<LearnItem> learnItemList = new ArrayList<>();
        try {
            InputStream inputStream =  myFile.getInputStream();
            Workbook workbook = new XSSFWorkbook(inputStream);
            // lấy từ sheet 2 đến 7 là các sheet: nhập môn, từ mới, ngữ pháp, hội thoại, luyện tập, đề kiểm tra
            for (int i = 2; i < 8; i++) {
                Sheet sheet = workbook.getSheetAt(i);

                Iterator<Row> rowIterator = sheet.rowIterator();

                while (rowIterator.hasNext()) {

                    Row row = rowIterator.next();
                    System.out.println(row.getRowNum());

                    if (row.getRowNum() == 0 || row.getRowNum() == 1 || row.getRowNum() == 2) {
                        continue;// bỏ 3 dòng đầu là dòng tiêu đề
                    }
                    LearnItem learnItem = new LearnItem();
                    //thứ tự các cột: 0-khoá học, 1-đơn vị học tập, 2-mã item, 3-loại trang, 4-title1, 5-title2,
                    //6-delay_time1, 7-delay_time2, 8-delay_time3, 9-image1, 10-image_text1, 11-image2, 12-image_text2,
                    //13-image3, 14-image_text3, 15-image4, 16-image_text4, 17-correct_answer, 18-Score, 19-mã learning unit
                    //1 số sheet thêm 1 cột<bài học> không dùng gì đến thì xoá đi để ko ảnh hưởng đến code.

                    if (row.getCell(1).getStringCellValue().isEmpty()) {
                        break;//nếu không có đơn vị học tập thì chương trình sẽ kết thúc
                    }

                    FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();

                    //cột 2-mã item thì nên copy kết quả dán vào cột đó, không nên dể dạng công thức khi chạy code vì sẽ làm lỗi tràn bộ nhớ khi tính toán.
                    if (evaluator.evaluate(row.getCell(2)) != null){
                        learnItem.setId((int) evaluator.evaluate(row.getCell(2)).getNumberValue());
                    }else {
                        learnItem.setId(0);
                    }

                    //cột 19-mã learning unit thì nên copy kết quả dán vào cột đó, không nên dể dạng công thức khi chạy code vì sẽ làm lỗi tràn bộ nhớ khi tính toán.
                    if (evaluator.evaluate(row.getCell(19)) != null){
                        learnItem.setLearnUnitId((int) evaluator.evaluate(row.getCell(19)).getNumberValue());
                    }else{
                        learnItem.setLearnUnitId(0);
                    }


                    if (evaluator.evaluate(row.getCell(18)) != null) {
                        learnItem.setScore((int) evaluator.evaluate(row.getCell(18)).getNumberValue());
                    } else {
                        learnItem.setScore(0);
                    }


                    if (row.getCell(3) != null) {
                        learnItem.setType(row.getCell(3).getStringCellValue());
                    } else {
                        learnItem.setType("");
                    }


                    JSONObject jsonObject = new JSONObject();// tạo đối tượng json lưu các thông tin còn lại

                    if (evaluator.evaluate(row.getCell(4)) != null) {
                        jsonObject.put("title1", evaluator.evaluate(row.getCell(4)).getStringValue());
                    } else {
                        jsonObject.put("title1", "");
                    }


                    if (evaluator.evaluate(row.getCell(5)) != null) {
                        jsonObject.put("title2", evaluator.evaluate(row.getCell(5)).getStringValue());
                    } else {
                        jsonObject.put("title2", "");
                    }


                    if (row.getCell(9) != null) {
                        jsonObject.put("image1", row.getCell(9).getStringCellValue());
                    } else {
                        jsonObject.put("image1", "");
                    }


                    if (row.getCell(10) != null) {
                        jsonObject.put("image_text1", row.getCell(10).getStringCellValue());
                    } else {
                        jsonObject.put("image_text1", "");
                    }


                    if (row.getCell(11) != null) {
                        jsonObject.put("image2", row.getCell(11).getStringCellValue());
                    } else {
                        jsonObject.put("image2", "");
                    }


                    if (row.getCell(12) != null) {
                        jsonObject.put("image_text2", row.getCell(12).getStringCellValue());
                    } else {
                        jsonObject.put("image_text2", "");
                    }


                    if (row.getCell(13) != null) {
                        jsonObject.put("image3", row.getCell(13).getStringCellValue());
                    } else {
                        jsonObject.put("image3", "");
                    }


                    if (row.getCell(14) != null) {
                        jsonObject.put("image_text3", row.getCell(14).getStringCellValue());
                    } else {
                        jsonObject.put("image_text3", "");
                    }


                    if (row.getCell(15) != null) {
                        jsonObject.put("image4", row.getCell(15).getStringCellValue());
                    } else {
                        jsonObject.put("image4", "");
                    }


                    if (row.getCell(16) != null) {
                        jsonObject.put("image_text4", row.getCell(16).getStringCellValue());
                    } else {
                        jsonObject.put("image_text4", "");
                    }


                    if (evaluator.evaluate(row.getCell(17)) != null) {
                        if(row.getCell(17).getCellTypeEnum() == CellType.BOOLEAN){
                            jsonObject.put("correct_answer", evaluator.evaluate(row.getCell(17)).getBooleanValue());
                        }else if (row.getCell(17).getCellTypeEnum() == CellType.NUMERIC){
                            jsonObject.put("correct_answer", (int) evaluator.evaluate(row.getCell(17)).getNumberValue());
                        }else {
                            jsonObject.put("correct_answer", evaluator.evaluate(row.getCell(17)).getStringValue());
                        }
                    } else {
                        jsonObject.put("correct_answer", "");
                    }

                    learnItem.setContent(jsonObject.toJSONString());

                    learnItemList.add(learnItem);
                }
            }
            workbook.close();
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return learnItemList;
    }
}
