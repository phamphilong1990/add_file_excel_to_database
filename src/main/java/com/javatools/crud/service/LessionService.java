package com.javatools.crud.service;

import com.javatools.crud.model.Lession;
import com.javatools.crud.repository.LessionRepo;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Slf4j
@Service
public class LessionService {

    @Autowired
    private LessionRepo lessionRepo;


    public List<Lession> saveLessionList(List<Lession> lessionList) {
        return lessionRepo.saveAll(lessionList);
    }

    public String deleteAllLession() {
        lessionRepo.deleteAll();
        return "delete all";
    }

    public List<Lession> readLessionFromExcel(MultipartFile myFile){
        List<Lession> lessionList = new ArrayList<>();

        try {
            InputStream inputStream =  myFile.getInputStream();
            Workbook workbook = new XSSFWorkbook(inputStream);
            Sheet sheet = workbook.getSheetAt(0);// sheet 0 là sheet < danh sách bài học >
            Iterator<Row> rowIterator = sheet.rowIterator();

            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();

                if (row.getRowNum() == 0||row.getRowNum() == 1) {
                    continue;// dòng 0 và 1 là dòng tiêu đề nên bỏ qua
                }
                Lession lession = new Lession();
                // thứ tự các cột : 0-khoá học, 1-tên tiếng nhật, 2-tên tiếng viêt, 3-ảnh minh hoạ, 4-kiểu bài học, 5-mã bài học
                String courseName = row.getCell(0).getStringCellValue();
                int courseId = 0;
                switch (courseName){
                    case "Nhập môn":
                        courseId = 1;// qui ước khoá học nhập môn là số 1
                        break;
                    case "N5":
                        courseId = 2;// qui ước khoá học N5 là số 2
                        break;
                    case "N4":
                        courseId = 3;// qui ước khoá học N4 là số 3
                        break;
                    case "Chuyên nghành":
                        courseId = 4;// qui ước khoá học Chuyên nghành là số 4
                        break;
                    case "Định hướng":
                        courseId = 5;// qui ước khoá học Định hướng là số 5
                        break;
                    default:
                        break;
                }
                if(courseId == 0){
                    break;//nếu không có khoá học nào thì sẽ thoát chương trình
                }
                lession.setCourseId(courseId);
                lession.setNameNativeLanguage(row.getCell(1).getStringCellValue());
                lession.setNameSecondLanguage(row.getCell(2).getStringCellValue());

                if(row.getCell(3) != null){
                    lession.setImage(row.getCell(3).getStringCellValue());
                }else{
                    lession.setImage("");
                }

                if(row.getCell(4) != null){
                    lession.setType(row.getCell(4).getStringCellValue());
                }else{
                    lession.setType("");
                }


                FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
                lession.setId((int) evaluator.evaluate(row.getCell(5)).getNumberValue());

                lessionList.add(lession);
            }
            workbook.close();
            inputStream.close();
        }catch (IOException e){
            e.printStackTrace();
        }
            return lessionList;
    }
}
