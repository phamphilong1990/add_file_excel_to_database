package com.javatools.crud.service;


import com.javatools.crud.model.LearnUnit;
import com.javatools.crud.model.Lession;
import com.javatools.crud.repository.LearnUnitRepo;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Slf4j
@Service
public class LearnUnitService {

    @Autowired
    private LearnUnitRepo learnUnitRepo;

    public List<LearnUnit> saveLearnUnitList(List<LearnUnit> learnUnitList) {
        return learnUnitRepo.saveAll(learnUnitList);
    }

    public String deleteAllLearnUnit() {
        learnUnitRepo.deleteAll();
        return "delete all";
    }

    public List<LearnUnit> readLearnUnitFromExcel(MultipartFile myFile){
        List<LearnUnit> learnUnitList = new ArrayList<>();
        try{
            InputStream inputStream =  myFile.getInputStream();
            Workbook workbook = new XSSFWorkbook(inputStream);

            Sheet sheet = workbook.getSheetAt(1);// sheet 1 là sheet < Đơn vị học tập >

            Iterator<Row> rowIterator = sheet.rowIterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                //thứ tự các cột: 0-khoá học, 1-tên tiếng nhật, 2-bài học, 3-tên tiếng việt, 4-loại đơn vị học tập
                //5-mã đơn vị học tập, 6-mã bài học

                if (row.getRowNum() == 0||row.getRowNum() == 1||row.getRowNum() == 2) {
                    continue;//bỏ 3 dòng tiêu đề
                }
                LearnUnit learnUnit = new LearnUnit();
                if(row.getCell(1).getStringCellValue().isEmpty()){
                    break;//nếu cột <tên tiếng nhật> không có thì chương trình sẽ ngừng
                }

                FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
                learnUnit.setId((int) evaluator.evaluate(row.getCell(5)).getNumberValue());
                learnUnit.setLessionId((int) evaluator.evaluate(row.getCell(6)).getNumberValue());
                learnUnit.setType(row.getCell(4).getStringCellValue());
                learnUnit.setNameNativeLanguage(row.getCell(1).getStringCellValue());

                if (row.getCell(3) != null) {
                    learnUnit.setNameForgeinLanguage(row.getCell(3).getStringCellValue());
                } else {
                    learnUnit.setNameForgeinLanguage("");
                }

                learnUnitList.add(learnUnit);
            }
            workbook.close();
            inputStream.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        return learnUnitList;
    }
}
